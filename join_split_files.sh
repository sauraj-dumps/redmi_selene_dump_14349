#!/bin/bash

cat system_ext/priv-app/MiuiFreeformService/MiuiFreeformService.apk.* 2>/dev/null >> system_ext/priv-app/MiuiFreeformService/MiuiFreeformService.apk
rm -f system_ext/priv-app/MiuiFreeformService/MiuiFreeformService.apk.* 2>/dev/null
cat system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system_ext/priv-app/Settings/Settings.apk
rm -f system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system_ext/apex/com.android.vndk.v30.apex
rm -f system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat product/data-app/SmartHome/SmartHome.apk.* 2>/dev/null >> product/data-app/SmartHome/SmartHome.apk
rm -f product/data-app/SmartHome/SmartHome.apk.* 2>/dev/null
cat product/data-app/MiShop/MiShop.apk.* 2>/dev/null >> product/data-app/MiShop/MiShop.apk
rm -f product/data-app/MiShop/MiShop.apk.* 2>/dev/null
cat product/data-app/MiuiScanner/MiuiScanner.apk.* 2>/dev/null >> product/data-app/MiuiScanner/MiuiScanner.apk
rm -f product/data-app/MiuiScanner/MiuiScanner.apk.* 2>/dev/null
cat product/data-app/MIpay_NO_NFC/MIpay_NO_NFC.apk.* 2>/dev/null >> product/data-app/MIpay_NO_NFC/MIpay_NO_NFC.apk
rm -f product/data-app/MIpay_NO_NFC/MIpay_NO_NFC.apk.* 2>/dev/null
cat product/data-app/MIUIYoupin/MIUIYoupin.apk.* 2>/dev/null >> product/data-app/MIUIYoupin/MIUIYoupin.apk
rm -f product/data-app/MIUIYoupin/MIUIYoupin.apk.* 2>/dev/null
cat product/data-app/MIMediaEditor/MIMediaEditor.apk.* 2>/dev/null >> product/data-app/MIMediaEditor/MIMediaEditor.apk
rm -f product/data-app/MIMediaEditor/MIMediaEditor.apk.* 2>/dev/null
cat product/data-app/com.iflytek.inputmethod.miui/com.iflytek.inputmethod.miui.apk.* 2>/dev/null >> product/data-app/com.iflytek.inputmethod.miui/com.iflytek.inputmethod.miui.apk
rm -f product/data-app/com.iflytek.inputmethod.miui/com.iflytek.inputmethod.miui.apk.* 2>/dev/null
cat product/data-app/MIUIDuokanReader/MIUIDuokanReader.apk.* 2>/dev/null >> product/data-app/MIUIDuokanReader/MIUIDuokanReader.apk
rm -f product/data-app/MIUIDuokanReader/MIUIDuokanReader.apk.* 2>/dev/null
cat product/data-app/MIUINotes/MIUINotes.apk.* 2>/dev/null >> product/data-app/MIUINotes/MIUINotes.apk
rm -f product/data-app/MIUINotes/MIUINotes.apk.* 2>/dev/null
cat product/data-app/Health/Health.apk.* 2>/dev/null >> product/data-app/Health/Health.apk
rm -f product/data-app/Health/Health.apk.* 2>/dev/null
cat product/priv-app/MIUISecurityCenter/MIUISecurityCenter.apk.* 2>/dev/null >> product/priv-app/MIUISecurityCenter/MIUISecurityCenter.apk
rm -f product/priv-app/MIUISecurityCenter/MIUISecurityCenter.apk.* 2>/dev/null
cat product/priv-app/MIUIBrowser/MIUIBrowser.apk.* 2>/dev/null >> product/priv-app/MIUIBrowser/MIUIBrowser.apk
rm -f product/priv-app/MIUIBrowser/MIUIBrowser.apk.* 2>/dev/null
cat product/priv-app/Gallery_T_CN/Gallery_T_CN.apk.* 2>/dev/null >> product/priv-app/Gallery_T_CN/Gallery_T_CN.apk
rm -f product/priv-app/Gallery_T_CN/Gallery_T_CN.apk.* 2>/dev/null
cat product/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null >> product/priv-app/MiuiCamera/MiuiCamera.apk
rm -f product/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null
cat product/priv-app/MiuiExtraPhoto/MiuiExtraPhoto.apk.* 2>/dev/null >> product/priv-app/MiuiExtraPhoto/MiuiExtraPhoto.apk
rm -f product/priv-app/MiuiExtraPhoto/MiuiExtraPhoto.apk.* 2>/dev/null
cat product/priv-app/MIUIMusicT/MIUIMusicT.apk.* 2>/dev/null >> product/priv-app/MIUIMusicT/MIUIMusicT.apk
rm -f product/priv-app/MIUIMusicT/MIUIMusicT.apk.* 2>/dev/null
cat product/priv-app/MIUIVideo/MIUIVideo.apk.* 2>/dev/null >> product/priv-app/MIUIVideo/MIUIVideo.apk
rm -f product/priv-app/MIUIVideo/MIUIVideo.apk.* 2>/dev/null
cat product/app/VoiceAssistAndroidT/VoiceAssistAndroidT.apk.* 2>/dev/null >> product/app/VoiceAssistAndroidT/VoiceAssistAndroidT.apk
rm -f product/app/VoiceAssistAndroidT/VoiceAssistAndroidT.apk.* 2>/dev/null
cat product/app/CarWith/CarWith.apk.* 2>/dev/null >> product/app/CarWith/CarWith.apk
rm -f product/app/CarWith/CarWith.apk.* 2>/dev/null
cat product/app/OtaProvision/OtaProvision.apk.* 2>/dev/null >> product/app/OtaProvision/OtaProvision.apk
rm -f product/app/OtaProvision/OtaProvision.apk.* 2>/dev/null
cat product/app/TrichromeLibrary64/TrichromeLibrary64.apk.* 2>/dev/null >> product/app/TrichromeLibrary64/TrichromeLibrary64.apk
rm -f product/app/TrichromeLibrary64/TrichromeLibrary64.apk.* 2>/dev/null
cat product/app/WebViewGoogle64/WebViewGoogle64.apk.* 2>/dev/null >> product/app/WebViewGoogle64/WebViewGoogle64.apk
rm -f product/app/WebViewGoogle64/WebViewGoogle64.apk.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
